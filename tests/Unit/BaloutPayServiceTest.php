<?php

namespace Tests\Unit;

use App\Models\User;
use App\Services\Payment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BaloutPayServiceTest extends TestCase {
    use RefreshDatabase;

    public function test_making_pay() {

        $user = User::factory()->create();

        $gem_amount = 20;

        $payment = new Payment();
        $transaction = $payment->pay($user->id , $gem_amount , "");

        $this->assertEquals($gem_amount , $transaction->amount);
        $this->assertEquals($user->gem_amount, $transaction->previous_amount + $transaction->amount);

    }

}