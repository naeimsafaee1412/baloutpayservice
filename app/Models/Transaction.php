<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * @property int $previous_amount
 * @property int $amount
 * @property int $user_id
 * @property string $tag
 * @mixin Builder
 **/
class Transaction extends Model {
    use HasFactory;

    protected $fillable = [
        'previous_amount', 'amount', 'tag', 'user_id'
    ];

    protected $casts = [
        'previous_amount' => 'int', 'amount' => 'int'
    ];

}
