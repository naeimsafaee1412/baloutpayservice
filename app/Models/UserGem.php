<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $user_id
 * @property int $gem_amount
 * @mixin Builder
 **/
class UserGem extends Model {
    use HasFactory;

    protected $primaryKey = 'user_id';

    protected $table = 'user_gems';

    protected $fillable = [
        'user_id',
        'gem_amount'
    ];

}
