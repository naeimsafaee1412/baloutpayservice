<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property int $gem_amount
 * @mixin Builder
 **/
class User extends Authenticatable {
    use HasApiTokens, HasFactory, Notifiable;

    protected $appends = ['gem_amount'];

    public function getGemAmountAttribute() {
        return $this->gem ? $this->gem->gem_amount : 0;
    }

    public function gem() {
        return $this->hasOne(UserGem::class);
    }


}
