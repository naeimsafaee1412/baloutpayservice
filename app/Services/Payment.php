<?php

namespace App\Services;

use App\Models\Transaction;
use App\Models\User;
use App\Models\UserGem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Payment {

    private Model $transaction;

    public function __construct() {
    }

    /**
     * @throws \Exception
     */
    public function pay(int $userId, int $amount, string $tag): Model {

        DB::transaction(function () use($userId , $amount , $tag){

            UserGem::query()->where('user_id' , $userId)->lockForUpdate();

            $user = User::query()->find($userId);

            $this->transaction = Transaction::query()->create([
                'previous_amount' => $user->gem_amount,
                'amount' => $amount,
                'tag' => $tag,
                'user_id' => $userId
            ]);

            UserGem::query()->updateOrCreate([
                'user_id' => $this->transaction->user_id,
            ],[
                'user_id' => $this->transaction->user_id,
                'gem_amount' => DB::raw('gem_amount + ' . $this->transaction->amount)
            ]);

        }, 5);

        return $this->transaction;
    }

}