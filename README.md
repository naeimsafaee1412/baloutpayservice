
## About Balout pay

Just do the following:

#### First: 

- Copy and paste Transaction model and migration
- Gem field in users is : gem_amount
- Use Payable trait in your user model
- Then:

```
php artisan migrate
```

#### For testing:

```
php artisan db:seed
php artisan test
```