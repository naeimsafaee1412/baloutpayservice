<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGemAmountsTable extends Migration {

    public function up() {
        Schema::create('user_gems', function(Blueprint $table) {
            $table->unsignedBigInteger('user_id')->unique();
            $table->integer('gem_amount');
            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete("cascade");
        });

    }

    public function down() {
        Schema::dropIfExists('user_gem_amounts');
    }
}
